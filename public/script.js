$(document).ready(function() {
	$('.multiple-items').slick({
	  nextArrow: '<button type="button" class="slick-next"><img src="next.png" alt=""></button>',
   	  prevArrow: '<button type="button" class="slick-prev"><img src="prev.png" alt=""></button>',
	  dots: true,
	  dotsClass: 'dots-style',
	  infinite: true,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [
	   {
	    breakpoint: 600,
	    settings: {
	    slidesToShow: 2,
	    slidesToScroll: 2
	    }
	   }
	 ]
   });
});